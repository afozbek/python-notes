# Python Notlarım
- [*Virtual Environment Kurma*](https://docs.python.org/3/tutorial/venv.html)
- [Windows Dizinde Environment Kurma](https://programwithus.com/learn-to-code/Pip-and-virtualenv-on-Windows/)


## Jhango Notlarım
- [Jhango için Environment Kurulumu](https://docs.djangoproject.com/en/2.1/howto/windows/)
    > Hızlı  setup kurulumu

    1. İlk Önce [**virtualenvwrapper-win**](https://pypi.org/project/virtualenvwrapper-win/) paketini indirelim

        `pip install virtualenvwrapper-win`

    2. Sonrasında bir environment oluşturalım

        `mkvirtualenv myproject`

    3. Environment' ı aktif hale getirmek için ise

        `workon myproject` 
- [Django için baslangic kaynagi](https://docs.djangoproject.com/en/2.1/intro/tutorial01/)